﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CategoryListTestSample.Core.Services;
using CategoryListTestSample.Core.Models;
using System.Collections.Generic;

namespace CategoryList_TestSample.Test
{
    [TestClass]
    public class PersonCategoryUnitTest
    {
        [TestMethod]
        public void GetPersonsTest()
        {
            PersonService objPersonService = new PersonService();
            var val = objPersonService.GetPersons();
            Assert.IsNotNull(val);

        }

        [TestMethod]
        public void FetchJsonTest()
        {
            var uriBuilder = new UriBuilder("http://agl-developer-test.azurewebsites.net/people.json");

            var Response = HTTPClient.FetchJson(uriBuilder.Uri);
            Assert.IsNotNull(Response);

        }

        [TestMethod]
        public  void GetPersons()
        {
            var wpclient = new HTTPClient();
            var response= wpclient.GetResponse<Person>();
            Assert.IsNotNull(response);
        }
    }
}
