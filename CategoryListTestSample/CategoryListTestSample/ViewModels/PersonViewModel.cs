﻿using CategoryListTestSample.Core.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CategoryListTestSample.ViewModels
{
    public class PersonViewModel :   INotifyPropertyChanged
    {
        private List<PersonGroup> personMale;
        private List<PersonGroup> personFemale;
        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Holds Male Person List
        /// </summary>
        public List<PersonGroup> PersonMale
        {
            get
            {
                return this.personMale;
            }

            set
            {
                if (value != this.personMale)
                {
                    this.personMale = value;
                    NotifyPropertyChanged();
                }
            }
        }
        /// <summary>
        /// Holds Female Person List
        /// </summary>
        public List<PersonGroup> PersonFemale
        {
            get
            {
                return this.personFemale;
            }

            set
            {
                if (value != this.personFemale)
                {
                    this.personFemale = value;
                    NotifyPropertyChanged();
                }
           
            }
        }


        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
