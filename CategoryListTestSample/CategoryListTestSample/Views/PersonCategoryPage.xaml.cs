﻿using CategoryListTestSample.Core.Models;
using CategoryListTestSample.Core.Services;
using CategoryListTestSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace CategoryListTestSample.Views
{
    public partial class PersonCategoryPage : ContentPage
    {
        PersonViewModel viewModel = null;
        public PersonCategoryPage()
        {
            InitializeComponent();
            viewModel = new PersonViewModel();
            this.BindingContext = viewModel;
            LoadCategoryItems();
        }

        async Task LoadCategoryItems()
        {
            try
            {
                var persons = await new PersonService().GetPersons();

                if (persons != null)
                {
                    List<Person> Pers = ((List<Person>)persons).ToList();

                    List<Person> personFilteredList = new List<Person>();
                    personFilteredList = Pers
                                    .Where(c => c.pets != null)
                                    .Select(c => new Person()
                                    {
                                        name = c.name,
                                        gender = c.gender,
                                        age = c.age,
                                        pets = GetChildren(c.pets)
                                    })
                                    .ToList();

                    bindGroup(personFilteredList);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static List<Pet> GetChildren(List<Pet> comments)
        {
            return comments
                    .Where(c => c.type.ToLower().ToString() == "cat")
                    .Select(c => new Pet
                    {
                        name = c.name,
                        type = c.type
                    })
                    .ToList();
        }

        private void bindGroup(List<Person> personFilteredList)
        {
            List<PersonGroup> maleGroupColl = new List<PersonGroup>();
            List<PersonGroup> femaleGroupColl = new List<PersonGroup>();
            if (personFilteredList != null)
            {
                var maleList = personFilteredList.Where(f => f.gender.ToLower().Equals("male")).ToList();
                foreach (var male in maleList)
                {
                    var maleGroup = new PersonGroup() { HeaderDisplayName = male.name, HeaderShortName = male.name };
                    List<Pet> petGroup = new List<Pet>();
                    foreach (var pet in male.pets)
                    {
                        petGroup.Add(new Pet() { name = pet.name, type = pet.type });
                        maleGroup.Add(new Person() { name = pet.name });
                    }

                    maleGroupColl.Add(maleGroup);
                }
                viewModel.PersonMale = maleGroupColl;
                var femaleList = personFilteredList.Where(f => f.gender.ToLower().Equals("female")).ToList();
                foreach (var female in femaleList)
                {
                    var femaleGroup = new PersonGroup() { HeaderDisplayName = female.name, HeaderShortName = female.name };
                    List<Pet> petGroup = new List<Pet>();
                    foreach (var pet in female.pets)
                    {
                        petGroup.Add(new Pet() { name = pet.name, type = pet.type });
                        femaleGroup.Add(new Person() { name = pet.name });
                    }
                    femaleGroupColl.Add(femaleGroup);
                }
                viewModel.PersonFemale = femaleGroupColl;
            }

        }
    }
}
