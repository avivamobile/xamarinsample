﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CategoryListTestSample.Core.Helpers
{
    public static class HttpClientFactory
    {
        public static HttpClient GetInstance()
        {
            return new HttpClient(new LoadingMessageHandler());
        }

#if __MOBILE__
		public class LoadingMessageHandler:NativeMessageHandler
#else
        public class LoadingMessageHandler : HttpClientHandler
#endif
        {
            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
            {

                var task = base.SendAsync(request, cancellationToken);
                return task.ContinueWith(x =>
                {
                    if (x.IsFaulted)
                        throw x.Exception;
                    else
                        return x.Result;
                });
            }
        }

        public class LoadingHttpClient : HttpClient
        {

            public LoadingHttpClient(HttpMessageHandler handler) : base(handler) { }

            public LoadingHttpClient() { }

            public override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
            {

                var task = base.SendAsync(request, cancellationToken);

                return task.ContinueWith(x =>
                {
                    if (x.IsFaulted)
                        throw x.Exception;
                    else
                        return x.Result;
                });
            }


        }
    }
}

