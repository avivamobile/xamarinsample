﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CategoryListTestSample.Core.Helpers;

namespace CategoryListTestSample.Core.Services
{
   public  class HTTPClient
    {
        string HttpUrl;
        public HTTPClient()
        {
            this.HttpUrl = "http://agl-developer-test.azurewebsites.net/people.json";
        }
        /// <summary>
        /// GENERIC METHOD TO FECTH JSON REQUEST AND PARSES IT.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<IList<T>> GetResponse<T>()
        {
            var uriBuilder = new UriBuilder(HttpUrl);
            var json = await FetchJson(uriBuilder.Uri);
            var settings = new JsonSerializerSettings();
            settings.Error = delegate (object sender, ErrorEventArgs args)
            {
                //Handle or log errors as required
                args.ErrorContext.Handled = true;
            };
            return JsonConvert.DeserializeObject<IList<T>>(json, settings);
        }

      public  static async Task<string> FetchJson(Uri uri)
        {
            using (var httpClient = HttpClientFactory.GetInstance())
            {
                var response = await httpClient.GetAsync(uri);
                response.EnsureSuccessStatusCode();
                var json = await response.Content.ReadAsStringAsync();
                return json;
            }
        }
    }
}
