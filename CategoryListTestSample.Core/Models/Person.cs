﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace CategoryListTestSample.Core.Models
{
    public class Person
    {
        public string name { get; set; }
        public string gender { get; set; }
        public int age { get; set; }
        public List<Pet> pets { get; set; }
    }

    public class PersonGroup : ObservableCollection<Person>
    {
        public string HeaderDisplayName { get; set; }
        public string HeaderShortName { get; set; }

    }
}
